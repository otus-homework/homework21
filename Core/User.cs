﻿using System;

namespace Core
{
    public class User
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }

        public override string ToString()
        {
            return $"Имя: {Name}, Возраст: {Age}, Email: {Email}";
        }

    }
}
