﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace Core
{
    public class Broker: IDisposable
    {
        protected IConnection Connection { get; }
        protected IModel Channel { get; }
        protected string QueueName { get; }

        public Broker()
        {
            QueueName = ConfigurationManager.AppSettings["QueueName"];

            var factory = new ConnectionFactory()
            {
                HostName = ConfigurationManager.AppSettings["HostName"],
                VirtualHost = ConfigurationManager.AppSettings["VirtualHost"],
                UserName = ConfigurationManager.AppSettings["UserName"],
                Password = ConfigurationManager.AppSettings["Password"]
            };

            Connection = factory.CreateConnection();
            Channel = Connection.CreateModel();
            Channel.QueueDeclare(queue: QueueName,
                                 durable: true,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
        }

        public void Dispose()
        {
            Channel.Close();
            Connection.Close();
        }
    }
}
