﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Core
{
    public class UserGenerator
    {
        private static int Count { get; set; } = 0;

        public static User GetUser()
        {
            var user = new User()
            {
                Name = Count.ToString(),
                Age = Count,
                Email =
                Count % 2 == 0 ? null : Count.ToString()
            };
            Count++;
            Thread.Sleep(1000);
            return user;
        }
    }
}
