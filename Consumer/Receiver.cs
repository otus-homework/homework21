﻿using Core;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.MessagePatterns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consumer
{
    class Receiver : Broker
    {
        public EventingBasicConsumer Consumer { get; }

        public Receiver() : base()
        {
            Consumer = new EventingBasicConsumer(Channel);
            Consumer.Received += Consumer_Received;
        }

        public void Read()
        {
            Channel.BasicConsume(queue: QueueName,
                                 autoAck: false,
                                 consumer: Consumer);
        }

        private void Consumer_Received(object sender, BasicDeliverEventArgs e)
        {
            var body = e.Body;
            var message = Encoding.UTF8.GetString(body);
            var user = JsonConvert.DeserializeObject<User>(message);

            Console.Write("Принято: {0}", message);
            if (!String.IsNullOrEmpty(user.Email))
            {
                Channel.BasicAck(e.DeliveryTag, false);
                Console.WriteLine(" - подтверждено!");
            }
            else
            {
                Console.WriteLine(" - не подтверждено");
            }
        }
    }
}
