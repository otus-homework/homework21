﻿using System;
using System.Timers;

namespace Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var receiver = new Receiver())
            {
                Console.WriteLine("Чтение очереди запущено!");
                using (var timer = new Timer(2000))
                {
                    timer.Elapsed += (sender, e) => receiver.Read();
                    timer.Start();

                    Console.ReadKey();
                    timer.Stop();
                }
            }
        }
    }
}
