﻿using Core;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Provider
{
    public class Sender : Broker
    {
        public Sender() : base() { }

        public void Publish(User user)
        {
            string message = JsonConvert.SerializeObject(user);
            var body = Encoding.UTF8.GetBytes(message);

            Channel.BasicPublish(exchange: "",
                routingKey: QueueName,
                basicProperties: null,
                body: body);
        }
    }
}
