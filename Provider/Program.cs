﻿using Core;
using System;

namespace Provider
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var sender = new Sender())
            {
                Console.WriteLine("Запись в очередь запущена!");
                while (true)
                {
                    sender.Publish(UserGenerator.GetUser());
                }
            }
        }
    }
}
